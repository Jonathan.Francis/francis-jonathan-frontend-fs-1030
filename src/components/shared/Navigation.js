import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'


const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    return (
        
        <Navbar dark color="dark" expand="md" fixed="top" className="navbar">
            <Container>
            <NavbarBrand href="">Jonathan Francis</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem>
                       <NavLink tag={RouteLink} to="/Entry">Create Entry</NavLink>
                    </NavItem>
                    <NavItem>
                       <NavLink tag={RouteLink} to="/User">Create User</NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink tag={RouteLink} to="/Login">Login</NavLink>
                    </NavItem>
                </Nav>
            </Collapse> 
            </Container>
        </Navbar>
       
    )
}

export default Navigation