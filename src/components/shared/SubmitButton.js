import React from 'react'
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  button: {
    backgroundColor: "#632ce4",
    color: "#FFFFFF",
    marginBottom: 10
  }
})

const SubmitButtton = ({ text }) => {

  const classes = useStyles();

  return (
    <Button className={classes.button} color="primary" variant="contained" component="button" type="submit" fullWidth>{text || "Submit"}</Button>
  )
}

export default SubmitButtton