import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';



const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
  table: {
    width: '100%',
    marginBottom: 40,
  },
  dateHeader: {
    width: 205,
  },
  margin: {
    margin: "0 10px 20px 0",
    width: '130%'
  },
  subheading: {
    margin: "50px 5px 20px 0",
    textAlign: "left"
  },
});

const ResumeFeedbackFormListProfile = (props) =>{

let id = props.match.params.id;


const [savedRes, setSavedRes] = useState("");
const [feedbacks,setFeedbacks] = useState([]); 
const [form, setForm] = useState({ display: "none" });
const [feedback, setFeedback] = useState({ resumeEmployerName: "", resumeEmployerContact: "", resumeEmployerOpening: "", resumeEmployerFeedback: ""});
const history = useHistory();
const classes = useStyles();


const returnRoute = (event) =>{
  event.preventDefault();
  let path = `/Resume/FeedbackForm/List`;
  history.push(path);
};



useEffect(() => {
  async function fetchData() {
    const res = await fetch(`https://cloud-examples-d2byuuoema-pd.a.run.app/resume/${id}`);
    res.json().then((res) => setFeedbacks(res));
  }
  fetchData();
}, [savedRes, id]);


const handleDelete = (event) => {
  event.preventDefault();

  fetch(`https://cloud-examples-d2byuuoema-pd.a.run.app/resume/${id}`, {
    method: "delete",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  }).then((response) => response.json());
  history.push("/Resume/FeedbackForm/List");
};


const handleEdit = (event, feedback) => {
  event.preventDefault();
  setForm({ display: "block" });
  setFeedback(feedback);
};


const handleChange = e => setFeedback({...feedback,[e.target.name]: e.target.value});

const handleSubmit = (event, feedback) => {
  event.preventDefault();
  fetch(`https://cloud-examples-d2byuuoema-pd.a.run.app/resume/${id}`, {
    method: "put",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },

    //make sure to serialize your JSON body
    body: JSON.stringify(feedback),
  }).then((response) => setSavedRes(response));
};



return (
 <div> 
<h1>Resume Feedback Profile</h1>
<button onClick={(e) => returnRoute(e)}>Return</button>
<TableContainer className={classes.table} component={Paper}>
    <Table aria-label="simple table">
        <TableHead>
            <TableRow>
            <TableCell>ID:</TableCell>
            <TableCell>Employer Name:</TableCell>
            <TableCell>Employer Contact Information:</TableCell>
            <TableCell>Job Opening/Opportunity: </TableCell>
            <TableCell>Employer Feedback: </TableCell>
            <TableCell>Date:  </TableCell>
            <TableCell>Employer Entry ID </TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {feedbacks.map((feedback) =>(
                <TableRow key={feedback.resumeEmployerID}>
                    <TableCell>{feedback.resumeEmployerID}</TableCell>
                    <TableCell>{feedback.resumeEmployerName}</TableCell>
                    <TableCell>{feedback.resumeEmployerContact}</TableCell>
                    <TableCell>{feedback.resumeEmployerOpening}</TableCell>
                    <TableCell>{feedback.resumeEmployerFeedback}</TableCell>
                    <TableCell>{feedback.resumeEmployerDate.toString().slice(0,10)}</TableCell>
                    <TableCell>{feedback.entryID}</TableCell>
                    <TableCell><Button variant="contained" color="primary" onClick={(e) => {handleEdit(e, feedback);}} component={Paper}>Edit</Button></TableCell>
                    <TableCell><Button  variant="contained" color="secondary" onClick={(e) => {handleDelete(e);}} component={Paper}>Delete</Button></TableCell>
                </TableRow>
            ))}
        </TableBody>
    </Table>
</TableContainer>


    {/* This is the form that pops up when you press the Edit Me! button */}
    <form style={form}>
    <div>
            <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Employer Name:"
              type="text"
              name="resumeEmployerName"
              value={feedback.resumeEmployerName}
              onChange={handleChange}
            />
        </div>
        <div>
        <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Employer Contact Information:"
              type="text"
              name="resumeEmployerContact"
              value={feedback.resumeEmployerContact}
              onChange={handleChange}
            />
        </div>
        <div>
          <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Job Opening/Opportunity:"
              type="text"
              name="resumeEmployerOpening"
              value={feedback.resumeEmployerOpening}
              onChange={handleChange}
            />
        </div>
        <div>
          <TextField 
              fullWidth 
              className={classes.textfield}
              label="Employer Feedback:"
              variant="outlined"
              type="text"
              name="resumeEmployerFeedback"
              value={feedback.resumeEmployerFeedback}
              onChange={handleChange}
            />
        </div>
        
        <Button className={classes.textfield} fullWidth variant="contained" color="primary" onClick={(e) => handleSubmit(e, feedback)} component={Paper}>Submit</Button>
        <Button className={classes.textfield} fullWidth onClick={() => {setForm({ display: "none" })}}>Cancel</Button>
      </form>
  </div>
);


}


export default ResumeFeedbackFormListProfile;


