import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SubmitButtton from "../shared/SubmitButton"

const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
});


const PortfolioFeedbackForm = () => {
     
     const classes = useStyles();
     const [feedback, setFeedback] = useState({portfolioEmployerContact:"", portfolioEmployerFeedback:"", portfolioEmployerRating:"", entryID:""});
     const history = useHistory();
     
     const returnRoute = (event) =>{
      event.preventDefault();
      let path = `/Portfolio`;
      history.push(path);
    };
    

     const handleChange = e => setFeedback({...feedback,[e.target.name]: e.target.value});


    const handleSubmit = (event) => {
        fetch(`https://cloud-examples-d2byuuoema-pd.a.run.app/portfolio`, {
         method: "post",
         headers: {
         Accept: "application/json",
        "Content-Type": "application/json",
      },

           body: JSON.stringify(feedback),
        }).then((response) => response.json());
        history.push("/Portfolio/FeedbackForm/List");
      };



  return (
    <>
      <h2>Employer Feedback on Portfolio</h2>
      <button onClick={(e) => returnRoute(e)}>Return</button>
      <br></br>
      <br></br>
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField fullWidth className={classes.textfield} id="addEmployerContact" label="Contact" variant="outlined" type="text" name="portfolioEmployerContact" value={feedback.portfolioEmployerContact} onChange={handleChange} required/>
        <TextField fullWidth className={classes.textfield} id="addEmployerFeedback" label="Feedback" variant="outlined" type="text" name="portfolioEmployerFeedback" value={feedback.portfolioEmployerFeedback}  onChange={handleChange} required/>     
        <TextField fullWidth className={classes.textfield} id="addEmployerRating" label="Rating" variant="outlined" type="text" name="portfolioEmployerRating" value={feedback.portfolioEmployerRating}  onChange={handleChange} required/> 
        <TextField fullWidth className={classes.textfield} id="addEmployerEntryID" label="Employer Entry ID" variant="outlined" type="text" name="entryID" value={feedback.entryID}  onChange={handleChange} required/>     
        <SubmitButtton />
      </form>
    </>
  );
};

export default PortfolioFeedbackForm;
