import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    table: {
      width: '100%'
    },
  });



const ResumeFeedbackFormList = (props)=>{

const [feedbacks,setFeedbacks] = useState([]);
const history = useHistory();
const classes = useStyles();


const resumeRoute = (event) =>{
  event.preventDefault();
  let path = "/Resume"
  history.push(path);

};

const portfolioRoute = (event) =>{
  event.preventDefault();
  let path = "/Portfolio"
  history.push(path);

};


const feedbackProfileRoute = (event, feedback) => {
    event.preventDefault();
    let path = `/Resume/FeedbackForm/List/${feedback.resumeEmployerID}`;
    history.push(path);
  };

useEffect(() => {
    async function fetchData() {
      const res = await fetch(`https://cloud-examples-d2byuuoema-pd.a.run.app/resume`);
      res
        .json()
        .then((res) => setFeedbacks(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);

  return (
    
    <div>
<h1>Resume Feedback Master List</h1>
<button onClick={(e) => resumeRoute(e)}>Return to Resume Page</button>
<button onClick={(e) => portfolioRoute(e)}>Visit Portfolio Page</button>
<TableContainer className={classes.table} component={Paper}>
    <Table aria-label="simple table">
        <TableHead>
            <TableRow>
            <TableCell>ID:</TableCell>
            <TableCell>Employer Name:</TableCell>
            <TableCell>Employer Contact Information:</TableCell>
            <TableCell>Job Opening/Opportunity: </TableCell>
            <TableCell>Employer Feedback: </TableCell>
            <TableCell>Date:  </TableCell>
            <TableCell>Employer Entry ID </TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {feedbacks.map((feedback) =>(
                <TableRow key={feedback.resumeEmployerID}>
                    <TableCell>{feedback.resumeEmployerID}</TableCell>
                    <TableCell>{feedback.resumeEmployerName}</TableCell>
                    <TableCell>{feedback.resumeEmployerContact}</TableCell>
                    <TableCell>{feedback.resumeEmployerOpening}</TableCell>
                    <TableCell>{feedback.resumeEmployerFeedback}</TableCell>
                    <TableCell>{feedback.resumeEmployerDate.toString().slice(0,10)}</TableCell>
                    <TableCell>{feedback.entryID}</TableCell>
                    <TableCell><Button color="primary" variant="contained" onClick={(e) => feedbackProfileRoute(e, feedback)} component={Paper}>View Profile</Button></TableCell>
                </TableRow>
            ))}
        </TableBody>
    </Table>
</TableContainer>
</div>


      );
}



export default ResumeFeedbackFormList;