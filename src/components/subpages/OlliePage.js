import React from "react";
import ollie1 from "../../assets/visual/ollie2.jpg"
import ollie2 from "../../assets/visual/ollie3.jpg"
import ollie3 from "../../assets/visual/ollie4.jpg"
import ollie4 from "../../assets/visual/ollie5.jpg"



function OlliePage(){

  return (

        <div>
            
            <body className="ollie-pg-body">
        
        <header>
            <h1 className="ollie-pg-h1">Ollie's Gallery</h1>
        
        </header>
    
    <main>
    
    
        <div className="ollie-pg-container">      
            <div className="ollie-pg-row">
            <div className="ollie-pg-column">
               <img className="ollie-img1" src={ollie1} alt="Ollie, the cat, sitting on the window ledge"/>
            </div>
            <div className="ollie-pg-column">
                <img className="ollie-img2" src={ollie2} alt="Ollie, the cat, sitting on a green chair in the corner of a room"/>
            </div>
        </div> 
        
        <div className="ollie-pg-row">
            <div className="ollie-pg-column">
              <img className="ollie-img3" src={ollie3} alt="Ollie, the cat, outdoors sitting under a shade of tree sleeping on the ground looking up"/>
            </div>
            <div className="ollie-pg-column">
              <img className="ollie-img4" src={ollie4} alt="Ollie, the cat, sitting on top of the coffee table looking at the photographer"/>
            </div>
        </div>
      </div>   
   
    </main>
    
    
    <footer>
      
        <h5 className="ollie-pg-h5">***MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW MEOW***</h5>
    </footer>
    
    </body>
</div>

        )
}

export default OlliePage;